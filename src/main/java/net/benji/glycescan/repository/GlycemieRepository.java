package net.benji.glycescan.repository;

import net.benji.glycescan.record.Glycemie;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface GlycemieRepository extends ElasticsearchRepository<Glycemie, Double> {
}
