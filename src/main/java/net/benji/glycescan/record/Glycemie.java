package net.benji.glycescan.record;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.time.LocalDateTime;

@Document(indexName = "glycemie-#{T(java.time.LocalDate).now().toString()}")
public class Glycemie {

    @Id
    private String id;

    @Field(type = FieldType.Date, format = DateFormat.basic_date_time)
    private LocalDateTime heureData;

    @Field(type = FieldType.Float)
    private float typeEnregistrement;

    @Field(type = FieldType.Float)
    private float tauxGlucoseHistorique;

    @Field(type = FieldType.Float)
    private float tauxGlucoseScanne;

    @Field(type = FieldType.Float)
    private float insulineRapide;

    @Field(type = FieldType.Float)
    private float unitesInsulineRapide;

    @Field(type = FieldType.Float)
    private float nourriture;

    @Field(type = FieldType.Float)
    private float glucides;

    @Field(type = FieldType.Float)
    private float insulineLente;

    @Field(type = FieldType.Float)
    private float unitesInsulineLente;

    @Field(type = FieldType.Text)
    private String commentaires;

    @Field(type = FieldType.Float)
    private float glycemieElectrodeDosage;

    @Field(type = FieldType.Float)
    private float cetonemie;

    @Field(type = FieldType.Float)
    private float insulineRepas;

    @Field(type = FieldType.Float)
    private float insulineCorrection;

    @Field(type = FieldType.Float)
    private float insulineModifiee;

    @Field(type = FieldType.Date, format = DateFormat.basic_date_time)
    private LocalDateTime heurePrecedente;

    @Field(type = FieldType.Date, format = DateFormat.basic_date_time)
    private LocalDateTime heureMiseAJour;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDateTime getHeureData() {
        return heureData;
    }

    public void setHeureData(LocalDateTime heureData) {
        this.heureData = heureData;
    }

    public float getTypeEnregistrement() {
        return typeEnregistrement;
    }

    public void setTypeEnregistrement(float typeEnregistrement) {
        this.typeEnregistrement = typeEnregistrement;
    }

    public float getTauxGlucoseHistorique() {
        return tauxGlucoseHistorique;
    }

    public void setTauxGlucoseHistorique(float tauxGlucoseHistorique) {
        this.tauxGlucoseHistorique = tauxGlucoseHistorique;
    }

    public float getTauxGlucoseScanne() {
        return tauxGlucoseScanne;
    }

    public void setTauxGlucoseScanne(float tauxGlucoseScanne) {
        this.tauxGlucoseScanne = tauxGlucoseScanne;
    }

    public float getInsulineRapide() {
        return insulineRapide;
    }

    public void setInsulineRapide(float insulineRapide) {
        this.insulineRapide = insulineRapide;
    }

    public float getUnitesInsulineRapide() {
        return unitesInsulineRapide;
    }

    public void setUnitesInsulineRapide(float unitesInsulineRapide) {
        this.unitesInsulineRapide = unitesInsulineRapide;
    }

    public float getNourriture() {
        return nourriture;
    }

    public void setNourriture(float nourriture) {
        this.nourriture = nourriture;
    }

    public float getGlucides() {
        return glucides;
    }

    public void setGlucides(float glucides) {
        this.glucides = glucides;
    }

    public float getInsulineLente() {
        return insulineLente;
    }

    public void setInsulineLente(float insulineLente) {
        this.insulineLente = insulineLente;
    }

    public float getUnitesInsulineLente() {
        return unitesInsulineLente;
    }

    public void setUnitesInsulineLente(float unitesInsulineLente) {
        this.unitesInsulineLente = unitesInsulineLente;
    }

    public String getCommentaires() {
        return commentaires;
    }

    public void setCommentaires(String commentaires) {
        this.commentaires = commentaires;
    }

    public float getGlycemieElectrodeDosage() {
        return glycemieElectrodeDosage;
    }

    public void setGlycemieElectrodeDosage(float glycemieElectrodeDosage) {
        this.glycemieElectrodeDosage = glycemieElectrodeDosage;
    }

    public float getCetonemie() {
        return cetonemie;
    }

    public void setCetonemie(float cetonemie) {
        this.cetonemie = cetonemie;
    }

    public float getInsulineRepas() {
        return insulineRepas;
    }

    public void setInsulineRepas(float insulineRepas) {
        this.insulineRepas = insulineRepas;
    }

    public float getInsulineCorrection() {
        return insulineCorrection;
    }

    public void setInsulineCorrection(float insulineCorrection) {
        this.insulineCorrection = insulineCorrection;
    }

    public float getInsulineModifiee() {
        return insulineModifiee;
    }

    public void setInsulineModifiee(float insulineModifiee) {
        this.insulineModifiee = insulineModifiee;
    }

    public LocalDateTime getHeurePrecedente() {
        return heurePrecedente;
    }

    public void setHeurePrecedente(LocalDateTime heurePrecedente) {
        this.heurePrecedente = heurePrecedente;
    }

    public LocalDateTime getHeureMiseAJour() {
        return heureMiseAJour;
    }

    public void setHeureMiseAJour(LocalDateTime heureMiseAJour) {
        this.heureMiseAJour = heureMiseAJour;
    }
}
