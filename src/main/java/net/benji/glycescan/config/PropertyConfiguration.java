package net.benji.glycescan.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:directories.properties")
public class PropertyConfiguration {
}
