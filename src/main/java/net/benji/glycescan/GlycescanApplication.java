package net.benji.glycescan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class GlycescanApplication {

    public static void main(String[] args) {
        SpringApplication.run(GlycescanApplication.class, args);
    }

}
