package net.benji.glycescan.service;

import net.benji.glycescan.record.Glycemie;

import java.util.List;

public interface IElasticService {

    List<Glycemie> saveGlycemie(List<Glycemie> dataToSave);

}
