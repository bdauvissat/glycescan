package net.benji.glycescan.service.impl;

import net.benji.glycescan.record.Glycemie;
import net.benji.glycescan.repository.GlycemieRepository;
import net.benji.glycescan.service.IElasticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service("elasticService")
public class ElasticServiceImpl implements IElasticService {

    private GlycemieRepository glycemieRepository;

    @Override
    public List<Glycemie> saveGlycemie(List<Glycemie> dataToSave) {

        return StreamSupport.stream(glycemieRepository.saveAll(dataToSave).spliterator(), false)
                .collect(Collectors.toList());

    }

    @Autowired
    public void setGlycemieRepository(GlycemieRepository glycemieRepository) {
        this.glycemieRepository = glycemieRepository;
    }
}
