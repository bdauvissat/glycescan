package net.benji.glycescan.service.impl;

import net.benji.glycescan.record.Glycemie;
import net.benji.glycescan.service.ILectureFichierGlycemie;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service("lectureFichierGlycemie")
public class LectureFichierGlycemieImpl implements ILectureFichierGlycemie {

    @Value("${local.path.glycemie}")
    private String path;

    @Value("${local.path.glycemie.done}")
    private String done;

    @Override
    public List<Glycemie> importFichier() {

        List<Glycemie> retour = new ArrayList<>();

        File repertoire = new File(path);

        if (!repertoire.isDirectory()) {
            return retour;
        }

        List<File> fichiersDuRepertoire = Arrays.asList(Objects.requireNonNull(repertoire.listFiles()));

        if (CollectionUtils.isEmpty(fichiersDuRepertoire)) {
            return retour;
        }

        for (File fichier:fichiersDuRepertoire) {
            retour.addAll(lectureFichier(fichier));
        }

        return retour;
    }

    private List<Glycemie> lectureFichier(File fichier) {

        List<Glycemie> lignesDuFichier = new ArrayList<>();

        if (fichier.isDirectory()) {
            return lignesDuFichier;
        }

        try {
            FileInputStream fis = new FileInputStream(fichier);

            InputStreamReader isr = new InputStreamReader(fis, StandardCharsets.UTF_8);
            BufferedReader br = new BufferedReader(isr);

            String line;
            int numLigne = 0;

            while ( ( line = br.readLine() ) != null )
            {
                String[] ligne = line.split("\t", -1);
                numLigne++;

                Glycemie record = transformeLigne(ligne, numLigne);

                if (record != null) {
                    lignesDuFichier.add(record);
                }
            }

            fis.close();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // Déplacement du fichier
            File destination = new File(path + "/" + done);
            boolean existeDone = destination.exists();

            if (!existeDone) {
                existeDone = destination.mkdir();
            }

            if (existeDone) {

                boolean move = fichier.renameTo(new File(path + "/" + done + "/" + fichier.getName()));

                System.out.println("Déplacement : " + move);

            }

        }

        return lignesDuFichier;

    }

    private Glycemie transformeLigne(String[] ligne, int numLigne) {

        if (Arrays.stream(ligne).count() < 19) {
            return null;
        }

        if (ligne[0].equals("ID")) {
            return null;
        }

        Glycemie valeur = new Glycemie();

        valeur.setId(ligne[0] + numLigne);
        valeur.setHeureData(parseDateMemeVide(ligne[1]));
        valeur.setTypeEnregistrement(parseFloatMemeVide(ligne[2]));
        valeur.setTauxGlucoseHistorique(parseFloatMemeVide(ligne[3]));
        valeur.setTauxGlucoseScanne(parseFloatMemeVide(ligne[4]));
        valeur.setInsulineRapide(parseFloatMemeVide(ligne[5]));
        valeur.setUnitesInsulineRapide(parseFloatMemeVide(ligne[6]));
        valeur.setNourriture(parseFloatMemeVide(ligne[7]));
        valeur.setGlucides(parseFloatMemeVide(ligne[8]));
        valeur.setInsulineLente(parseFloatMemeVide(ligne[9]));
        valeur.setUnitesInsulineLente(parseFloatMemeVide(ligne[10]));
        valeur.setCommentaires(ligne[11]);
        valeur.setGlycemieElectrodeDosage(parseFloatMemeVide(ligne[12]));
        valeur.setCetonemie(parseFloatMemeVide(ligne[13]));
        valeur.setInsulineRepas(parseFloatMemeVide(ligne[14]));
        valeur.setInsulineCorrection(parseFloatMemeVide(ligne[15]));
        valeur.setInsulineModifiee(parseFloatMemeVide(ligne[16]));
        valeur.setHeurePrecedente(parseDateMemeVide(ligne[17]));
        valeur.setHeureMiseAJour(parseDateMemeVide(ligne[18]));

        return valeur;

    }

    private float parseFloatMemeVide(String val) {
        if (StringUtils.isEmpty(val.trim())) {
            return 0f;
        }
        
        return Float.parseFloat(val.trim().replace(',', '.'));
        
    }

    private LocalDateTime parseDateMemeVide(String val) {

        if (StringUtils.isEmpty(val.trim())) {
            return null;
        }

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyy/MM/dd HH:mm");
        return LocalDateTime.parse(val, dtf);

    }

}
