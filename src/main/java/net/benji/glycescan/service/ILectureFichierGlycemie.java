package net.benji.glycescan.service;

import net.benji.glycescan.record.Glycemie;

import java.util.List;

public interface ILectureFichierGlycemie {

    List<Glycemie> importFichier();

}
