package net.benji.glycescan.componnent;

import net.benji.glycescan.record.Glycemie;
import net.benji.glycescan.service.IElasticService;
import net.benji.glycescan.service.ILectureFichierGlycemie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Component
public class LectureFichiers {

    private ILectureFichierGlycemie lectureFichierGlycemie;

    private IElasticService elasticService;

    @Scheduled(cron="* */2 * * * *") // Lancement toutes les deux minutes
    public void lectureGlycemie() {

        List<Glycemie> data = lectureFichierGlycemie.importFichier();

        if (CollectionUtils.isEmpty(data)) {
            return;
        }

        elasticService.saveGlycemie(data);

    }

    @Autowired
    public void setLectureFichierGlycemie(ILectureFichierGlycemie lectureFichierGlycemie) {
        this.lectureFichierGlycemie = lectureFichierGlycemie;
    }

    @Autowired
    public void setElasticService(IElasticService elasticService) {
        this.elasticService = elasticService;
    }
}
