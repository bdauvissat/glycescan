package net.benji.glycescan.service.impl;

import net.benji.glycescan.GlycescanApplication;
import net.benji.glycescan.record.Glycemie;
import net.benji.glycescan.service.ILectureFichierGlycemie;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GlycescanApplication.class)
class LectureFichierGlycemieImplTest {

    @Autowired
    private LectureFichierGlycemieImpl lectureFichierGlycemie;

    @Test
    void importFichier() {

        List<Glycemie> retour = lectureFichierGlycemie.importFichier();

        assertNotNull(retour);
        assertNotEquals(0, retour.size());
        assertEquals(11377, retour.size());

    }
}
